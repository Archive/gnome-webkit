/*
 * Copyright (C) 2003 Apple Computer, Inc.  All rights reserved.
 * Copyright (C) 2004 Ian McKellar <yakk@yakk.net> All rights reserved.
 * Copyright (C) 2004 Xan Lopez <xan@gnome.org> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY APPLE COMPUTER, INC. ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL APPLE COMPUTER, INC. OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */

#include "KWQFont.h"

#include "KWQExceptions.h"
#include "KWQString.h"

static PangoFontDescription* default_font_description = NULL;

static PangoFontDescription* get_default_font_description ()
{
    if (default_font_description == NULL) {
	default_font_description = pango_font_description_new ();
	pango_font_description_set_family (default_font_description, "Serif");
	pango_font_description_set_size (default_font_description, 12); // Value from Apple
	pango_font_description_set_weight (default_font_description, PANGO_WEIGHT_NORMAL);
    }

    return pango_font_description_copy (default_font_description);
}

	
QFont::QFont()
    :fontdesc(get_default_font_description ())
{
}

QFont::~QFont()
{
    pango_font_description_free (fontdesc);
}

QFont::QFont(const QFont &other)
{
    fontdesc = pango_font_description_copy (other.fontdesc);
}

QFont &QFont::operator=(const QFont &other)
{
    pango_font_description_free (fontdesc);
    fontdesc = pango_font_description_copy (other.fontdesc);
    return *this;
}

QString QFont::family() const
{
    return QString(pango_font_description_get_family (fontdesc));
}

void QFont::setFamily(const QString &qfamilyName)
{
    pango_font_description_set_family (fontdesc, qfamilyName.latin1());
}

void QFont::setFirstFamily(const KWQFontFamily& family) 
{
    pango_font_description_set_family (fontdesc, family.family());
}

/* This code comes from Galeon, src/migrate.c */

#define INT_ROUND(a) (gint)((a) + 0.5f)

/**
 *  This function gets the dpi in the same way that mozilla gets the dpi, 
 *  this allows us to convert from pixels to points easily
 */
static gint
mozilla_get_dpi ()
{
	GtkSettings* settings = gtk_settings_get_default ();
	gint dpi = 0;
	char *val;
	float screenWidthIn;

	/* Use the gtk-xft-dpi setting if it is set */
	if (g_object_class_find_property (G_OBJECT_GET_CLASS (G_OBJECT (settings)),
					  "gtk-xft-dpi"))
	{
		g_object_get (G_OBJECT (settings), "gtk-xft-dpi", &dpi, NULL);
		if (dpi) return INT_ROUND (dpi / PANGO_SCALE);
	}

	/* Fall back to what xft thinks it is */
	val = XGetDefault (GDK_DISPLAY (), "Xft", "dpi");
	if (val)
	{
		char *e;
		double d = strtod(val, &e);
		if (e != val) return INT_ROUND (d);
	}
	
	/* Fall back to calculating manually from the gdk screen settings */
	screenWidthIn = ((float)gdk_screen_width_mm()) / 25.4f;
	return INT_ROUND (gdk_screen_width() / screenWidthIn);
}

/* This seems to be unused 
void QFont::setPixelSize(float s)
{
    if (_size != s) {
        [_NSFont release]; 
        _NSFont = 0;
    }
    _size = s;
}*/

/* FIXME: Not at all sure this is ok */
int pixelSize() const
{
    gint sys_ppi = mozilla_get_dpi ();
    gint font_dots = pango_font_description_get_size (fontdesc)/PANGO_SCALE;
    return (font_dots/72)*sys_ppi;
}

void QFont::setWeight(int weight)
{
    if (weight == Bold) {
	pango_font_description_set_weigth (fontdesc, PANGO_WEIGHT_BOLD);
    } else if (weight == Normal) {
	pango_font_description_set_weigth (fontdesc, PANGO_WEIGHT_NORMAL);
    }
}

int QFont::weight() const
{
    return bold() ? Bold : Normal;
}

void QFont::setItalic(bool flag)
{
    pango_font_description_set_style (fontdesc,
	    flag?PANGO_STYLE_ITALIC:PANGO_STYLE_NORMAL);
}

bool QFont::italic() const
{
    return pango_font_description_get_style (fontdesc) == PANGO_STYLE_ITALIC;
}

bool QFont::bold() const
{
    return pango_font_description_get_style (fontdesc) == PANGO_STYLE_BOLD;
}

bool QFont::isFixedPitch() const
{
    /* FIXME Do something? */
    return false;
}


bool QFont::operator==(const QFont &compareFont) const
{
    return pango_font_description_equal (fontdesc, other.fontdesc);
}

