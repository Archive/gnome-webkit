/*
 * Copyright (C) 2003 Apple Computer, Inc.  All rights reserved.
 * Copyright (C) 2004 Christian Meyer <chrisime@gnome.org>. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY APPLE COMPUTER, INC. ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL APPLE COMPUTER, INC. OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */

#include "KWQResourceLoader.h"

#include "KWQAssertions.h"
#include "KWQKJobClasses.h"
#include "loader.h"

using khtml::Loader;
using KIO::TransferJob;

KWQResourceLoader::KWQResourceLoader (TransferJob *job)
{
    _job = job;
    _job->setLoader(this);
}

void KWQResourceLoader::setHandle (void *handle)
{
    ASSERT (_handle == 0);
    _handle = handle;
}

/* FIXME */
void KWQResourceLoader::receiveRespone (NSURLResponse *response)
{
    ASSERT (response);
    ASSERT (_job);
    _job->emitReceiveResponse (response);
}

void KWQResourceLoader::redirectedToURL (KURL *url)
{
    ASSERT (url);
    ASSERT (_job);
    _job->emitRedirection (KURL (url));
}

/* FIXME */
void KWQResourceLoader::addData (NSData *data)
{
    ASSERT(data);
    ASSERT(_job);
    _job->emitData((const char *) data->bytes, data->length);
}

void KWQResourceLoader::jobWillBeDeallocated ()
{
    _job = 0;
    _handle->cancel ();
    delete _handle;
}

void KWQResourceLoader::finishJobAndHandle ()
{
    if (_job) {
        _job->emitResult ();
    }
    delete _job;
    delete _handle;
}

void KWQResourceLoader::cancel ()
{
    if (_job) {
        _job->setError (1);
    }
    finishJobAndHandle ();
}

void KWQResourceLoader::reportError ()
{
    ASSERT (_job);
    _job->setError (1);
    finishJobAndHandle ();
}

void KWQResourceLoader::finish ()
{
    ASSERT (_job);
    ASSERT (_handle);
    finishJobAndHandle ();
}
