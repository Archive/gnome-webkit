/*
 * Copyright (C) 2003 Apple Computer, Inc.  All rights reserved.
 * Copyright (C) 2003 Ian McKellar <yakk@yakk.net>  All rights reserved.
 * Copyright (C) 2004 Xan Lopez <xan@gnome.org> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY APPLE COMPUTER, INC. ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL APPLE COMPUTER, INC. OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */

#include "KWQRegion.h"

QRegion::QRegion(const QRect &rect)
{
    GdkRectangle r;

    r.x = rect.x();
    r.y = rect.y();
    r.width = rect.width();
    r.height = rect.height();

    region = gdk_region_new (&r);
}

QRegion::QRegion(int x, int y, int w, int h, RegionType t)
{
    if (t == Rectangle) {
	QRegion(QRect(x,y,w,h));
    } else { // Ellipse
	/* FIXME IMPLEMENT */
        //path = [[NSBezierPath bezierPathWithOvalInRect:NSMakeRect(x, y, w, h)] retain];
    }
}

QRegion::QRegion(const QPointArray &arr)
{
    GdkPoints *points;
    int length;

    points = arr.gdkPoints (&length); // REVISE
    region = gdk_region_polygon (points, length);

    g_free (points);
}

QRegion::~QRegion()
{
    gdk_region_destroy (region);
}

QRegion::QRegion(const QRegion &other)
{
    region = gdk_region_copy (other.region);
}

QRegion &QRegion::operator=(const QRegion &other)
{
    if (gdk_region_equal (region, other.region)) {
        return *this;
    }
    gdk_region_destroy (region);
    region = gdk_region_copy (other.region);
    return *this;
}

bool QRegion::contains(const QPoint &point) const
{
    return gdk_region_point_in (region, point.x(), point.y());
}

void QRegion::translate(int deltaX, int deltaY)
{
    gdk_region_offset (region, deltaX, deltaY);
}

QRect QRegion::boundingRect() const
{
    GdkRectangle r;

    gdk_region_get_clipbox (region, &r);

    return QRect(r);
}
