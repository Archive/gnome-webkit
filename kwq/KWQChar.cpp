/*
 * Copyright (C) 2003 Apple Computer, Inc.  All rights reserved.
 * Copyright (C) 2004 Xan Lopez <xan@gnome.org> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY APPLE COMPUTER, INC. ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL APPLE COMPUTER, INC. OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */

#include "KWQString.h"
#include "KWQLogging.h"

#include <glib/gunicode.h>

bool QChar::isDigit() const
{
    return g_unichar_isdigit (c);
}

bool QChar::isLetter() const
{
    return g_unichar_isalpha (c);
}

bool QChar::isNumber() const
{
    return isLetterOrNumber() && !isLetter();
}

bool QChar::isLetterOrNumber() const
{
    return g_unichar_is_alnum (c);
}

bool QChar::isPunct() const
{
    return g_unichar_is_punct (c);
}

QChar QChar::lower() const
{
    return g_unichar_tolower (c);
}

QChar QChar::upper() const
{
    return g_unichar_toupper (c);
}

bool QChar::mirrored() const
{
    gunichar ch;

    if (g_unichar_get_mirror_char (c, &ch)) {
        return TRUE;
    } else {
        return FALSE;
    }
}

QChar QChar::mirroredChar() const
{
    gunichar ch;

    if (g_unichar_get_mirror_char (c, &ch)) {
        return QChar(ch);
    } else {
        return QChar();
    }
}

int QChar::digitValue() const
{
    if (c < '0' || c > '9')
        return -1;
    else
        return c - '0';
}
