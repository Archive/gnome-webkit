/*
 * Copyright (C) 2003 Apple Computer, Inc.  All rights reserved.
 * Copyright (C) 2004 Xan Lopez <xan@gnome.org> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY APPLE COMPUTER, INC. ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL APPLE COMPUTER, INC. OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */

#include "KWQDateTime.h"
#include <time.h>

/* Current absolute time (since UNIX Epoch), in seconds */

static double getCurrentTime ()
{
    time_t t;
    time(&t);

    return (double)t;
}

/* We want to store the number of seconds since Epoch to last midnight
 * plus the seconds in the parameters. I hope. */

QTime::QTime(int hours, int minutes)
{
    time_t t;
    struct tm *tms;

    time (&t);
    tms = gmtime (&t);
    
    /* Seconds past midnight */
    double sec_pm = (double)(tm->tm_hour*3600)+(tm->tm_min*60)+tm->tm_sec;
    /* Seconds until last midnight */
    double sec_ulm = (double)t - sec_pm;
    
    timeInSeconds = sec_ulm + (double)((hours*60)+minutes)*60;
}

int QTime::msec() const
{
    return (int)(timeInSeconds * 1000) % 1000;
}

int QTime::elapsed()
{
    double elapsed = getCurrentTime () - timeInSeconds;
    return (int)(elapsed * 1000);
}

int QTime::restart()
{
    double currentTime = getCurrentTime ();
    double elapsed = currentTime - timeInSeconds;

    timeInSeconds = currentTime;
    return (int)(elapsed * 1000);
}

QDate::QDate(int y, int m, int d)
    : year(y), month(m), day(d)
{
}

QDateTime::QDateTime(const QDate &d, const QTime &t)
{
    struct tm *tms;
    double time_in_secs = t.timeInSeconds;

    tms = gmtime ((time_t*)&time_in_secs);

    tms->tm_year = d.year;
    tms->tm_month = d.month;
    tms->tm_day = d.day;

    dateInSeconds = (double)mktime (tms);
}

int QDateTime::secsTo(const QDateTime &b) const
{
    return (int)(b.dateInSeconds - dateInSeconds);
}

#ifdef _KWQ_IOSTREAM_

std::ostream &operator<<(std::ostream &o, const QDate &date)
{
    return o <<
        "QDate: [yy/mm/dd: " <<
        date.year <<
        '/' <<
        date.month <<
        '/' <<
        date.day <<
        ']';
}

std::ostream &operator<<(std::ostream &o, const QTime &time)
{
    struct tm *tms;

    tms = gmtime (time.timeInSeconds);
    return o <<
        "QTime: [hh:mm:ss:ms = " <<
        (int)tms->tm_hour <<
        ':' <<
        (int)tms->tm_min <<
        ':' <<
        (int)tm->sec <<
        ':' <<
        time.msec() <<
        ']';
}

std::ostream &operator<<(std::ostream &o, const QDateTime &dateTime)
{
    struct tm *tms;

    tms = gmtime (dateTime.dateInSeconds);
    return o <<
        "QDateTime: [yy/mm/dd hh:mm:ss:ms = " <<
        (int)tm->year <<
        '/' <<
        (int)tm->month <<
        '/' <<
        (int)tm->day <<
        ' ' << 
        (int)tm->hour <<
        ':' <<
        (int)tm->min <<
        ':' <<
        (int)tm->sec <<
        ':' <<
        ((int)(tm->sec * 1000) % 1000) <<
        ']';
}

#endif // _KWQ_IOSTREAM_
