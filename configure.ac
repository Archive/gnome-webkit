dnl Copyright (c) 2004 Christian Persch
dnl
dnl This program is free software; you can redistribute it and/or modify it
dnl under the terms of the GNU General Public License as published by the
dnl Free Software Foundation; either version 2.1 of the License, or (at your
dnl option) any later version.
dnl
dnl This program is distributed in the hope that it will be useful, but
dnl WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
dnl General Public License for more details.
dnl
dnl You should have received a copy of the GNU Lesser General Public License along
dnl with this program; if not, write to the Free Software Foundation, Inc.,
dnl 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
dnl
dnl $Id: configure.ac,v 1.2 2004/08/16 21:23:55 chpe Exp $

AC_INIT([GNOME WebKit], [0.0.0], [epiphany-list@gnome.org],[gnome-webkit])
GNOME_COMMON_INIT

GNOME_WEBKIT_MAJOR=0.0
AC_SUBST([GNOME_WEBKIT_MAJOR])

AC_PREREQ([2.57])

AC_REVISION([$Revision: 1.2 $])

AC_CONFIG_SRCDIR([kwq])
AM_CONFIG_HEADER([config.h])

AM_INIT_AUTOMAKE

AM_MAINTAINER_MODE

AC_PROG_INTLTOOL([0.29])

AM_DISABLE_STATIC
AC_ENABLE_SHARED([yes])
AC_ENABLE_STATIC([no])

AM_PROG_LIBTOOL

AC_ISC_POSIX
AC_PROG_CC
AC_PROG_CXX
AC_PROG_CPP
AC_PROG_AWK
AC_PROG_YACC
AC_PROG_LEX

dnl AC_PATH_PROG([GLIB_GENMARSHAL], [glib-genmarshal])

AC_SUBST([AM_CXXFLAGS])

GNOME_DEBUG_CHECK
dnl GNOME_COMPILE_WARNINGS([error])

dnl ********************
dnl Internationalisation
dnl ********************

ALL_LINGUAS=""

GETTEXT_PACKAGE=gnome-webkit-0
AC_SUBST([GETTEXT_PACKAGE])
AC_DEFINE_UNQUOTED([GETTEXT_PACKAGE],["$GETTEXT_PACKAGE"], [Gettext package])
AM_GLIB_GNU_GETTEXT

dnl ****
dnl PCRE
dnl ****

AC_PATH_PROG([PCRE_CONFIG], [pcre-config], [no])

if test "${PCRE_CONFIG}" = "no"; then
  AC_ERROR([pcre development files not found])
fi

PCRE_CFLAGS="`$PCRE_CONFIG --cflags`"
AC_SUBST([PCRE_CFLAGS])
PCRE_LIBS="`$PCRE_CONFIG --libs`"
AC_SUBST([PCRE_LIBS])

dnl *****
dnl Expat
dnl *****

dnl FIXME:
dnl Do we need to use the included expat, since we cannot guarantee that the system
dnl expat is the correct one (UTF-8 vs. UTF-16) ?

AC_CHECK_HEADERS([expat.h])

dnl *****
dnl gperf
dnl *****

AC_PATH_PROG([GPERF], [gperf], [no])

if test "${GPERF}" = "no"; then
  AC_ERROR([gperf not found])
fi

dnl *****************
dnl pkg-config checks
dnl *****************

GLIB_REQUIRED=2.4.0
PANGO_REQUIRED=1.4.0
GTK_REQUIRED=2.4.0
GNOMEVFS_REQUIRED=2.2.3

AC_SUBST([GLIB_REQUIRED])
AC_SUBST([PANGO_REQUIRED])
AC_SUBST([GTK_REQUIRED])
AC_SUBST([GNOMEVFS_REQUIRED])

PKG_CHECK_MODULES([KWQ_MODULES], \
		  glib-2.0 >= $GLIB_REQUIRED \
		  pango >= $PANGO_REQUIRED \
		  gtk+-2.0 >= $GTK_REQUIRED \
		  gnome-vfs-2.0 >= $GNOMEVFS_REQUIRED)
AC_SUBST([KWQ_MODULES_CFLAGS])
AC_SUBST([KWQ_MODULES_LIBS])

dnl ************
dnl Misc defines
dnl ************

dnl AC_DEFINE_UNQUOTED([APPLE_CHANGES],[1],[Define for Apple changes])
dnl AC_DEFINE_UNQUOTED([EPHY_CHANGES],[1],[Define for Epiphany changes])

dnl ***************************************************************************

AC_CONFIG_FILES([
Makefile
include/Makefile
include/css/Makefile
include/dom/Makefile
include/ecma/Makefile
include/html/Makefile
include/java/Makefile
include/kio/Makefile
include/kjs/Makefile
include/kparts/Makefile
include/misc/Makefile
include/private/Makefile
include/rendering/Makefile
include/xml/Makefile
kjs/Makefile
kwq/Makefile
khtml/Makefile
khtml/css/Makefile
khtml/dom/Makefile
khtml/ecma/Makefile
khtml/html/Makefile
khtml/misc/Makefile
khtml/rendering/Makefile
khtml/xml/Makefile
tests/Makefile
po/Makefile.in
])

AC_OUTPUT
